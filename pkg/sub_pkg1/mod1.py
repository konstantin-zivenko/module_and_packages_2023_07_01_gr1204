def foo():
    print("[mod1] foo()")


class Foo:
    pass


if __name__ == "__main__":
    a = Foo()
    assert isinstance(a, Foo), f"{a}, type"
    print("I`m working! mod1")