s = "It is my first module! It is lie..."
a = [11, 2, 3000]

__sss = "duble special name"

_ddd = "special name"


def pretty_print(*arg):
    print("-" * 80)
    print(*arg)
    print("-" * 80)


class Foo:
    pass


pretty_print(__name__)


if __name__ == "__main__":
    pretty_print(s, a, __name__)
